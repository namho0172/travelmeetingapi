package com.nh.travelmeetingapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Applicant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "packageID",nullable = false)
    private PackageId packageId;

    @Column(nullable = false)// 현재 대기 인지 확정인지 상태 확인
    private Boolean isConfirmation;

    @Column(nullable = false)//신청시간
    private LocalDateTime applicantDay;
}
