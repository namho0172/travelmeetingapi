package com.nh.travelmeetingapi.entity;

import com.nh.travelmeetingapi.enums.member.Grade;
import com.nh.travelmeetingapi.enums.member.State;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name; //맴버이름

    @Column(nullable = false)
    private Boolean isNormal; //일반 사용자 와 관리자 구분 ..현재 구분 불가.

    @Column(nullable = false,length = 20, unique = true)
    private String account; //아이디

    @Column(nullable = false)
    private String password; //비밀번호

    @Column(nullable = false)
    private LocalDate birthday; //생일

    @Column(nullable = false, length = 13, unique = true)
    private String phoneNumber; //전화번호

    @Column(nullable = false)
    private LocalDate dateJoin; // 가입일

//    @Column(nullable = false)
//    @Enumerated(EnumType.STRING)
//    private Gender gender;
    @Column(nullable = false)
    private Boolean isMan;

    @Column(nullable = false,length = 35)
    private String eMail; //이메일

    @Column(nullable = false, length = 100)
    private String address; //주소

    @Column(nullable = false, length = 15)
    @Enumerated(EnumType.STRING)
    private Grade grade; //등급

    private String reason;// 정지랑 정지사유 하나로 묶기

    @Column(nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private State state; // 활동중인지 아닌지.

    @Column(columnDefinition = "TEXT")
    private String etcMemo; //기타

}
