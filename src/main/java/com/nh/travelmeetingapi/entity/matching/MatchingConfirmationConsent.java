package com.nh.travelmeetingapi.entity.matching;

import com.nh.travelmeetingapi.entity.Applicant;
import com.nh.travelmeetingapi.entity.PackageId;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class MatchingConfirmationConsent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "applicantID",nullable = false)
    private Applicant applicant;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "packageID",nullable = false)
    private PackageId packageId;

    @Column(nullable = false)
    private LocalDate matchingStartDay;

    @Column(nullable = false)
    private LocalDate matchingEndDay;

    @Column(nullable = false)
    private String place;//장소


}
