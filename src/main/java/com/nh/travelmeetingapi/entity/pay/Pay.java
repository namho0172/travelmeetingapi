package com.nh.travelmeetingapi.entity.pay;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.entity.matching.MatchingConfirmationConsent;
import com.nh.travelmeetingapi.enums.pay.PayBy;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Pay {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "matchingId", nullable = false)
    private MatchingConfirmationConsent matchingConfirmationConsent; // 페키지id

    @Column(nullable = false, length = 50, unique = true)
    private String payCode; // 결제번호

    @Column(nullable = false)
    private LocalDate datePay; //결제일

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private PayBy payBy; // 결제수단

    @Column(nullable = false)
    private Boolean isCancel; // 취소여부
}
