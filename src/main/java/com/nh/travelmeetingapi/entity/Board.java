package com.nh.travelmeetingapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Board { // 게시판
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="memberId", nullable = false)
    private Member member; // 작성자

    @Column(length = 50)
    private String img;

    @Column(nullable = false)
    private LocalDateTime dateWrite; // 작성일

    @Column(nullable = false, length = 30)
    private String title; // 타이틀

    @Column(nullable = false, length = 1000)
    private String content; //컨탠츠
}
