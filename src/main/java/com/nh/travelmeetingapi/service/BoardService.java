package com.nh.travelmeetingapi.service;

import com.nh.travelmeetingapi.entity.Board;
import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.model.board.BoardCreateRequest;
import com.nh.travelmeetingapi.model.board.BoardItem;
import com.nh.travelmeetingapi.model.board.BoardResponse;
import com.nh.travelmeetingapi.repository.BoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;

    public void setBoard(Member member, BoardCreateRequest request) {
        Board addData = new Board();
        addData.setMember(member);
        addData.setImg(request.getImg());
        addData.setDateWrite(LocalDateTime.now());
        addData.setTitle(request.getTitle());
        addData.setContent(request.getContent());

        boardRepository.save(addData);
    }

    public List<BoardItem> getBoards() {
        List<Board> originList = boardRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1);

        List<BoardItem> result = new LinkedList<>();

        for (Board board : originList){
            BoardItem addItem = new BoardItem();
            addItem.setId(board.getId());
            addItem.setMemberName(board.getMember().getName());
            addItem.setImg(board.getImg());
            addItem.setDateWrite(board.getDateWrite());
            addItem.setTitle(board.getTitle());
            addItem.setContent(board.getContent());

            result.add(addItem);
        }

        return result;
    }

    public BoardResponse getBoard(long boardId) {
     Board originData = boardRepository.findById(boardId).orElseThrow();

     BoardResponse response = new BoardResponse();
     response.setId(originData.getId());
     response.setImg(originData.getImg());
     response.setMemberName(originData.getMember().getName());
     response.setDateWrite(originData.getDateWrite());
     response.setTitle(originData.getTitle());
     response.setContent(originData.getContent());

     return response;
    }

    public void delBoard(long boardId){
        boardRepository.deleteById(boardId);
    }
}
