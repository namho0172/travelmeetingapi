package com.nh.travelmeetingapi.service.matching;

import com.nh.travelmeetingapi.entity.Applicant;
import com.nh.travelmeetingapi.entity.PackageId;
import com.nh.travelmeetingapi.entity.matching.MatchingConfirmationConsent;
import com.nh.travelmeetingapi.model.MatchingConfirmationConsent.MatchingCreateRequest;
import com.nh.travelmeetingapi.model.MatchingConfirmationConsent.MatchingItem;
import com.nh.travelmeetingapi.model.MatchingConfirmationConsent.MatchingResponse;
import com.nh.travelmeetingapi.repository.matching.MatchingConfirmationConsentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MatchingConfirmationConsentService { //확정자 관리자 페이지 등록
    private final MatchingConfirmationConsentRepository matchingConfirmationConsentRepository;

    public MatchingConfirmationConsent getData(long id) {
        return matchingConfirmationConsentRepository.findById(id).orElseThrow();

    }

    public void setMatching(Applicant applicant, PackageId packageId, MatchingCreateRequest request) {
        // 같은 페키지들만 불러와
//        List<MatchingConfirmationConsent> listId = matchingConfirmationConsentRepository.findAllByPackageId_Id();

        MatchingConfirmationConsent addData = new MatchingConfirmationConsent();
        addData.setApplicant(applicant);
        addData.setPackageId(packageId);
        addData.setMatchingStartDay(request.getMatchingStartDay());
        addData.setMatchingEndDay(request.getMatchingEndDay());
        addData.setPlace(request.getPlace());

        matchingConfirmationConsentRepository.save(addData);
    }

    public List<MatchingItem> getMatchings(){
        List<MatchingConfirmationConsent> originList = matchingConfirmationConsentRepository.findAll();

        List<MatchingItem> result = new LinkedList<>();

        for (MatchingConfirmationConsent matchingConfirmationConsent : originList){
            MatchingItem addItem = new MatchingItem();
            addItem.setId(matchingConfirmationConsent.getId());
            addItem.setApplicant(matchingConfirmationConsent.getApplicant().getId());
            addItem.setPackageId(matchingConfirmationConsent.getPackageId().getId());
            addItem.setMatchingStartDay(matchingConfirmationConsent.getMatchingStartDay().toString());
            addItem.setMatchingEndDay(matchingConfirmationConsent.getMatchingStartDay().toString());
            addItem.setPlace(matchingConfirmationConsent.getPlace());


            result.add(addItem);
        }

        return result;
    }

    public MatchingResponse getMatching(long matchingId){
        MatchingConfirmationConsent originData = matchingConfirmationConsentRepository.findById(matchingId).orElseThrow();

        MatchingResponse response = new MatchingResponse();
        response.setId(originData.getId());
        response.setApplicant(originData.getApplicant().getId());
        response.setPackageId(originData.getPackageId().getId());
        response.setMatchingStartDay(originData.getMatchingEndDay().toString());
        response.setMatchingEndDay(originData.getMatchingStartDay().toString());
        response.setPlace(originData.getPlace());

        return response;
    }

    public void delMatching(long matchingId){

        matchingConfirmationConsentRepository.deleteById(matchingId);

    }
}
