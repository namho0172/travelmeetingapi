package com.nh.travelmeetingapi.service;


import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.entity.MyPage;
import com.nh.travelmeetingapi.entity.pay.Pay;
import com.nh.travelmeetingapi.model.myPage.MyPageRequest;
import com.nh.travelmeetingapi.repository.MypageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MyPageService {
    private final MypageRepository mypageRepository;

    public void setMyPage(Member member, Pay pay, MyPageRequest request){
        MyPage addData = new MyPage();
        addData.setMember(member);
        addData.setPay(pay);

        mypageRepository.save(addData);
    }

}
