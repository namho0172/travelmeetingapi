package com.nh.travelmeetingapi.service;

import com.nh.travelmeetingapi.enums.resuitCode.ResultCode;
import com.nh.travelmeetingapi.model.generic.CommonResult;
import com.nh.travelmeetingapi.model.generic.ListResult;
import com.nh.travelmeetingapi.model.generic.SingleResult;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResponseService {
    public static CommonResult getSuccessResult() { // 입력하는 포트용 post, put,del
        CommonResult result = new CommonResult();
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());

        return result;
    }
    public static <T> SingleResult<T> getSingleResult(T data){ // id 단수 포트용
        SingleResult<T> result = new SingleResult<>();
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());
        result.setData(data);

        return result;
    }
    public static <T> ListResult<T> getListResult(List<T> list){ //list 포트용
        ListResult<T> result = new ListResult<>();
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());
        result.setList(list);
        result.setTotalCount((long)list.size());

        return result;
    }
}
