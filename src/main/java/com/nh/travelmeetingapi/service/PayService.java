package com.nh.travelmeetingapi.service;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.entity.matching.MatchingConfirmationConsent;
import com.nh.travelmeetingapi.entity.pay.Pay;
import com.nh.travelmeetingapi.model.pay.PayChangRequest;
import com.nh.travelmeetingapi.model.pay.PayItem;
import com.nh.travelmeetingapi.model.pay.PayRequest;
import com.nh.travelmeetingapi.model.pay.PayResponse;
import com.nh.travelmeetingapi.repository.PayRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PayService {
    private final PayRepository payRepository;

    public Pay getData(Long id){return payRepository.findById(id).orElseThrow();}

    public void setPay(MatchingConfirmationConsent matchingConfirmationConsent, Member member, PayRequest request){
        Pay addData = new Pay();
        addData.setMember(member);
        addData.setMatchingConfirmationConsent(matchingConfirmationConsent);
        addData.setPayCode(request.getPayCode());
        addData.setDatePay(LocalDate.now());
        addData.setPayBy(request.getPayBy());
        addData.setIsCancel(false);

        payRepository.save(addData);
    }

    public List<PayItem> getPays(){
        List<Pay> originList = payRepository.findAll();

        List<PayItem> result = new LinkedList<>();

        for (Pay pay : originList){
            PayItem addList = new PayItem();
            addList.setId(pay.getId());
            addList.setMember(pay.getId());
            addList.setPayCode(pay.getPayCode());
            addList.setDatePay(pay.getPayCode());
            addList.setPayBy(pay.getPayCode());
            addList.setIsCancel(pay.getPayCode());

            result.add(addList);
        }
        return result;
    }
    public PayResponse getPay(long payId){
        Pay originData = payRepository.findById(payId).orElseThrow();

        PayResponse response = new PayResponse();
        response.setId(originData.getId());
        response.setMember(originData.getId());
        response.setPayCode(originData.getPayCode());
        response.setDatePay(originData.getDatePay().toString());
        response.setPayBy(originData.getPayBy().toString());
        response.setIsCancel(originData.getPayCode());

        return response;
    }
    public void putPayCancel(long payId, PayChangRequest request){
        Pay originData = payRepository.findById(payId).orElseThrow();
        originData.setIsCancel(request.getIsCancel());

        payRepository.save(originData);
    }
}
