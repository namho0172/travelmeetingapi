package com.nh.travelmeetingapi.service;


import com.nh.travelmeetingapi.entity.Applicant;
import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.entity.PackageId;
import com.nh.travelmeetingapi.model.applicant.ApplicantItem;
import com.nh.travelmeetingapi.model.applicant.ApplicantRequest;
import com.nh.travelmeetingapi.model.applicant.ApplicantResponse;
import com.nh.travelmeetingapi.repository.ApplicantRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ApplicantService {
    private final ApplicantRepository applicantRepository;
    // 매칭에서 내가 패키지를 선택해서 신청 하는 방법이 있다. 장점은 나의 자유의지로 선택하여 고른다는 장점이 있고 단점은 모집을 실패할 가능성이 있다.
    // 모집을 실패할 가능성을 줄이기 위해서 신청자가 매칭하기를 누르면 매칭을 받을 준비가 되었다고 인식하고 램덤으로 패키지를 지정해 준다.
    // 해당 패키지에 배당되어 참여를 하시겠습니까? 알람을 보내고 yes를 한다면 패키지 신청이 완료 된다.
    // 우리의 고객 대상은 20대 후반 30대 이상 50대 미만의 직장인들 사회에서 만남을 원하지만 만날 기회가 없는 남녀를 만남의 장소와 명분을 제공한다.
    // 나를 예로 들면 먼가 내가 직접 선택해서 하는 것 보다는 누군가 등떠미는 무언가가 있다면 조금의 용기로도 신청을 할 수 있을 것 같다.
    // 결국 우리는 만날 수 있는 환경과 명분 그리고 약간의 끌어줌을 제공하는 사업을 하는 것이다.

    public Applicant getData(Long id){return applicantRepository.findById(id).orElseThrow();}
    public void setApplicant(Member member, PackageId packageId, ApplicantRequest request) {

        Applicant addData = new Applicant();
        addData.setMember(member);
        addData.setPackageId(packageId);
        addData.setIsConfirmation(true);
        addData.setApplicantDay(LocalDateTime.now());

        applicantRepository.save(addData);
    }

    public List<ApplicantItem> getApps(){
        List<Applicant> originData = applicantRepository.findAll();

        List<ApplicantItem> result = new LinkedList<>();
        for (Applicant applicant1 : originData){
            ApplicantItem addList = new ApplicantItem();
            addList.setId(addList.getId());
            addList.setMember(applicant1.getId());
            addList.setMemberName(applicant1.getMember().getName());
            addList.setPackageId(applicant1.getId());
            addList.setPackageName(applicant1.getPackageId().getPackageName());
            addList.setIsConfirmation(applicant1.getIsConfirmation().toString());
            addList.setApplicantDay(applicant1.getApplicantDay().toString());

            result.add(addList);
        }
        return result;
    }
    public ApplicantResponse getApp(long id){
        Applicant originData = applicantRepository.findById(id).orElseThrow();

        ApplicantResponse response = new ApplicantResponse();
        response.setId(originData.getId());
        response.setMember(originData.getMember().getId());
        response.setMemberName(originData.getMember().getName());
        response.setPackageId(originData.getPackageId().getId());
        response.setPackageName(originData.getPackageId().getPackageName());
        response.setIsConfirmation(originData.getIsConfirmation().toString());
        response.setApplicantDay(originData.getApplicantDay().toString());

        return response;
    }

    public void delApp(long id){
        applicantRepository.deleteById(id);
    }

}
