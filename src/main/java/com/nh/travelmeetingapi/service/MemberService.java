package com.nh.travelmeetingapi.service;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.enums.member.Grade;
import com.nh.travelmeetingapi.enums.member.State;
import com.nh.travelmeetingapi.model.generic.ListResult;
import com.nh.travelmeetingapi.model.member.*;
import com.nh.travelmeetingapi.model.member.userChange.*;
import com.nh.travelmeetingapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;


@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(Long id) {return memberRepository.findById(id).orElseThrow();}

    public MemberDupCheckResponse getMemberIdDupCheck(String account){ // 팀안에서 서포터 하는 역활
        MemberDupCheckResponse request = new MemberDupCheckResponse();
        request.setIsNew(isNewMember(account));
        return request;
    }


    /**
     *
     * @param request 회원가입창에서 고객이 작성한 정보
     * @throws Exception 아이디 중복 확인 비밀번호랑 비밀번호 확인이 일치 하지 않을 경우
     */
    public void setMember(MemberJoinRequest request) throws Exception {
        // 중복확인 로직
        if (!isNewMember(request.getAccount())) throw new Exception();
        if (!request.getPassword().equals(request.getPasswordRe())) throw new Exception();
        // 중복확인 로직

        Member addData = new Member();
        addData.setName(request.getName());
        addData.setIsNormal(true);
        addData.setAccount(request.getAccount());
        addData.setPassword(request.getPassword());
        addData.setBirthday(request.getBirthday());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setDateJoin(LocalDate.now());
        addData.setIsMan(request.getIsMan());
        addData.setEMail(request.getEMail());
        addData.setAddress(request.getAddress());
        addData.setGrade(Grade.BRONZE);
        addData.setState(State.ACTIVITY);

        memberRepository.save(addData);
    }

    /**
     * 등록된 회원 정보들을 조회
     * @return
     */
    public List<MemberItem> getMembers(){
        List<Member> originList = memberRepository.findAll();
        List<MemberItem> result = new LinkedList<>();

        for (Member member : originList ) {
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setName(member.getName());
            addItem.setIsNormal(member.getIsNormal());
            addItem.setAccount(member.getAccount());
            addItem.setPassword(member.getPassword());
            addItem.setBirthday(member.getBirthday().toString());
            addItem.setPhoneNumber(member.getPhoneNumber());
            addItem.setDateJoin(member.getDateJoin().toString());
            addItem.setIsMan(member.getIsMan() ? "남자" : "여자");
            addItem.setEMail(member.getEMail());
            addItem.setAddress(member.getAddress());
            addItem.setGrade(member.getGrade().getName());
            addItem.setReason(member.getReason());
            addItem.setState(member.getState().getName());

            result.add(addItem);
        }
        return result;
    }

    /**
     * 등록된 회원 한명의 정보를 조회
     * @param id
     * @return
     */
    public MemberResponse getMember(long id){
        Member originData = memberRepository.findById(id).orElseThrow();

        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setAccount(originData.getAccount());
        response.setPassword(originData.getPassword());
        response.setBirthday(originData.getBirthday().toString());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setDateJoin(originData.getDateJoin().toString());
        response.setIsMan(originData.getIsMan() ? "남자" : "여자");
        response.setEMail(originData.getEMail());
        response.setAddress(originData.getAddress());
        response.setGrade(originData.getGrade().getName());
        response.setReason(originData.getReason());
        response.setState(originData.getState().getName());
        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }

    /**
     * 관리자가 회원들의 정보들을 수정
     * @param id
     * @param request
     */
    public void putMembers(long id, MemberChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setName(request.getName());
        originData.setIsNormal(request.getIsNormal());
        originData.setAccount(request.getAccount());
        originData.setPassword(request.getPassword());
        originData.setBirthday(request.getBirthday());
        originData.setPhoneNumber(request.getPhoneNumber());
        originData.setDateJoin(request.getDateJoin());
        originData.setIsMan(request.getIsMan());
        originData.setEMail(request.getEMail());
        originData.setAddress(request.getAddress());
        originData.setGrade(request.getGrade());
        originData.setReason(request.getReason());
        originData.setState(request.getState());
        originData.setEtcMemo(request.getEtcMemo());

        memberRepository.save(originData);
    }
    public void putMemberName(long id, MemberNameChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setName(request.getName());

        memberRepository.save(originData);
    }
    public void putMemberPw(long id, MemberPasswordChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setPassword(request.getPassword());

        memberRepository.save(originData);
    }
    public void putMemberPn(long id, MemberPhoneNumberChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setPhoneNumber(request.getPhoneNumber());

        memberRepository.save(originData);
    }
    public void putMemberEmail(long id, MemberEmailChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setEMail(request.getEMail());

        memberRepository.save(originData);
    }
    public void putMemberAd(long id, MemberAddressChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setAddress(request.getAddress());

        memberRepository.save(originData);
    }
    /**
     * 신규 아이디인지 확인한다
     *
     * @param account 아이디
     * @return  true 신규아이디 / false 중복아이디
     */
    private boolean isNewMember(String account){
        long dupCount = memberRepository.countByAccount(account);

        return dupCount <= 0;
    }
    public void delMember(long id){
        memberRepository.deleteById(id);
    }

    public ListResult<Member> getPages(int pageNum){
        PageRequest pageRequest = PageRequest.of(pageNum -1,9);
        Page<Member> memberPages = memberRepository.findAll(pageRequest);

        ListResult<Member> result = new ListResult<>();
        result.setList(memberPages.getContent());
        result.setTotalCount(memberPages.getTotalElements());
        result.setTotalPage(memberPages.getTotalPages());
        result.setCurrentPage(memberPages.getPageable().getPageNumber());

        return result;
    }
}
