package com.nh.travelmeetingapi.service;

import com.nh.travelmeetingapi.ilb.CommonFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

@Service
public class Tempservice {
    public void setRentCar(MultipartFile multipartFile) throws IOException {
        File file = CommonFile.mltipartToFile(multipartFile);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));//버퍼에 파일을 올려두고 버퍼안에 파일을 읽는다.

        // 버퍼에서 넘어온 string한줄을 임시로 담아줄 변수가 필요하다.
        String line = "";
        // 줄 번호 수동체크 하기 위해 int로 줄 번호 1식 증가해서 기록 해 둘 변수가 필요하다.
        int index = 0;

        while ((line = bufferedReader.readLine()) != null){
            if (index > 0){
              String[] cols = line.split(",");
              if (cols.length == 3){
                  System.out.println(cols[0]);
                  System.out.println(cols[1]);
                  System.out.println(cols[2]);

                  // 이제 db에 넣자.

              }

            }
            index++;
        }

        bufferedReader.close(); // 다 읽었으면 닫아 주어야 한다.

    }
}
