package com.nh.travelmeetingapi.service;

import com.nh.travelmeetingapi.entity.PackageId;
import com.nh.travelmeetingapi.model.Package.PackageItem;
import com.nh.travelmeetingapi.model.Package.PackageRequest;
import com.nh.travelmeetingapi.model.Package.PackageResponse;
import com.nh.travelmeetingapi.repository.PackageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PackageIdService {
    private final PackageRepository packageRepository;

    public PackageId getData(Long id){return packageRepository.findById(id).orElseThrow();}
    public void setPackage(PackageRequest request){
        PackageId addData = new PackageId();
        addData.setPackageName(request.getPackageName());

        packageRepository.save(addData);
    }

    public List<PackageItem> getPackages(){
        List<PackageId> originData = packageRepository.findAll();
        List<PackageItem> result = new LinkedList<>();

        for (PackageId packageId : originData){
            PackageItem addList = new PackageItem();
            addList.setId(packageId.getId());
            addList.setPackageName(packageId.getPackageName());

            result.add(addList);
        }
        return result;
    }
    public PackageResponse getPackageOne(long id){
        PackageId originData = packageRepository.findById(id).orElseThrow();

        PackageResponse response = new PackageResponse();
        response.setId(originData.getId());
        response.setPackageName(originData.getPackageName());

        return response;
    }

    public void delPackage(long id){
        packageRepository.deleteById(id);
    }
}
