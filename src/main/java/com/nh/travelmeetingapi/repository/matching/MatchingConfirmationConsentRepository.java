package com.nh.travelmeetingapi.repository.matching;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.entity.matching.MatchingConfirmationConsent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MatchingConfirmationConsentRepository extends JpaRepository<MatchingConfirmationConsent, Long> {

    List<MatchingConfirmationConsent> findAllByApplicant_Id(Long Id);
//    List<MatchingConfirmationConsent> findAllByApplicant_Member_GenderLike(Boolean gender);
}
