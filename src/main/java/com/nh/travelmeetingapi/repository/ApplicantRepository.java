package com.nh.travelmeetingapi.repository;

import com.nh.travelmeetingapi.entity.Applicant;
import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.entity.matching.MatchingConfirmationConsent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ApplicantRepository extends JpaRepository<Applicant, Long> {
}
