package com.nh.travelmeetingapi.repository;

import com.nh.travelmeetingapi.entity.pay.Pay;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PayRepository extends JpaRepository<Pay, Long> {
}
