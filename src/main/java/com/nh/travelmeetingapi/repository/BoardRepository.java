package com.nh.travelmeetingapi.repository;

import com.nh.travelmeetingapi.entity.Board;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BoardRepository extends JpaRepository<Board, Long> {
    List<Board> findAllByIdGreaterThanEqualOrderByIdDesc(long id);
}
