package com.nh.travelmeetingapi.model.board;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class BoardCreateRequest {
    private String img;
    private LocalDateTime dateWrite;
    private String title;
    private String content;
}
