package com.nh.travelmeetingapi.model.member;


import com.nh.travelmeetingapi.enums.member.Grade;
import com.nh.travelmeetingapi.enums.member.State;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class MemberChangeRequest {
    private String name;
    private Boolean isNormal;
    private String account;
    private String password;
    private LocalDate birthday;
    private String phoneNumber;
    private LocalDate dateJoin;
//    private Gender gender;
    private Boolean isMan;
    private String eMail;
    private String address;
    private Grade grade;
    private String reason;
    private State state;
    private String etcMemo;
}
