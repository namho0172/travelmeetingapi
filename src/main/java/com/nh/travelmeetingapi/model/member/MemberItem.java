package com.nh.travelmeetingapi.model.member;


import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class MemberItem {
    private Long id;

    private String name;

    private Boolean isNormal;

    private String account;

    private String password;

    private String birthday;

    private String phoneNumber;

    private String dateJoin;

    private String isMan;

    private String eMail;

    private String address;

    private String grade;

    private String reason;

    private String state;
}
