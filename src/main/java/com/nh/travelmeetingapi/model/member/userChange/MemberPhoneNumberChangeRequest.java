package com.nh.travelmeetingapi.model.member.userChange;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberPhoneNumberChangeRequest {
    private String phoneNumber;
}
