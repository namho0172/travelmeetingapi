package com.nh.travelmeetingapi.model.Package;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PackageResponse {
    private Long id;
    private String PackageName;
}
