package com.nh.travelmeetingapi.model.Package;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PackageRequest {
    private String PackageName;
}
