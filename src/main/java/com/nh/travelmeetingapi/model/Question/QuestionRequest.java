package com.nh.travelmeetingapi.model.Question;


import com.nh.travelmeetingapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuestionRequest {
    private String title;
    private String content;
}
