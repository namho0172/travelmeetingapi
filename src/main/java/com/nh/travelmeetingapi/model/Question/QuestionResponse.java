package com.nh.travelmeetingapi.model.Question;

import com.nh.travelmeetingapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class QuestionResponse {
    private Long id;
    private String memberName;
    private String title;
    private String content;
    private String inputDay;
}
