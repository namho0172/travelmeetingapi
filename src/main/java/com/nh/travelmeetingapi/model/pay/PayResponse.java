package com.nh.travelmeetingapi.model.pay;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.entity.matching.MatchingConfirmationConsent;
import com.nh.travelmeetingapi.enums.pay.PayBy;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PayResponse { // 하나의 구매내역만 불러오기
    private Long id;
    private Long member;
    private Long matchingConfirmationConsent;
    private String payCode;
    private String datePay;
    private String payBy;
    private String isCancel;
}
