package com.nh.travelmeetingapi.model.pay;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayChangRequest { // 고객이 구매 취소시 변경
    private Boolean isCancel; // 취소여부
}
