package com.nh.travelmeetingapi.model.pay;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.entity.matching.MatchingConfirmationConsent;
import com.nh.travelmeetingapi.enums.pay.PayBy;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PayRequest { // 기본적으로 고객에게 받을 것

    private Long id;
    private Member member;
    private MatchingConfirmationConsent matchingConfirmationConsent;
    private String payCode;
    private LocalDate datePay;
    @Enumerated(value = EnumType.STRING)
    private PayBy payBy;
    private Boolean isCancel;
}
