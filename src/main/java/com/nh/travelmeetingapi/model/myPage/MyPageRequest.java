package com.nh.travelmeetingapi.model.myPage;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.entity.pay.Pay;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MyPageRequest {
    private Member member;
    private Pay pay;
}
