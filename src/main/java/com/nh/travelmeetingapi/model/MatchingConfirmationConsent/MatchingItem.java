package com.nh.travelmeetingapi.model.MatchingConfirmationConsent;

import com.nh.travelmeetingapi.entity.Applicant;
import com.nh.travelmeetingapi.entity.PackageId;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class MatchingItem {
    private Long id;
    private Long applicant;
    private Long packageId;
    private String matchingStartDay;
    private String matchingEndDay;
    private String place;

}
