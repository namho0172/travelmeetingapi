package com.nh.travelmeetingapi.model.MatchingConfirmationConsent; // 에러걸진 적 있어서 매칭부분 변경했었음

import com.nh.travelmeetingapi.entity.Applicant;
import com.nh.travelmeetingapi.entity.PackageId;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class MatchingCreateRequest {

    private LocalDate matchingStartDay;
    private LocalDate matchingEndDay;
    private String place;

}
