package com.nh.travelmeetingapi.model.applicant;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.entity.PackageId;
import com.nh.travelmeetingapi.entity.matching.MatchingConfirmationConsent;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class ApplicantRequest {
    private LocalDateTime applicantDay;
}
