package com.nh.travelmeetingapi.controller;

import com.nh.travelmeetingapi.model.generic.CommonResult;
import com.nh.travelmeetingapi.service.ResponseService;
import com.nh.travelmeetingapi.service.Tempservice;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/temp")
public class TempController {
    private final Tempservice tempservice;

    @PostMapping("/file-upload")
    public CommonResult setRentCar(@RequestParam("csvFile")MultipartFile csvFile) throws IOException{
        tempservice.setRentCar(csvFile);
        return ResponseService.getSuccessResult();
    }
}
