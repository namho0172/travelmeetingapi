package com.nh.travelmeetingapi.controller;

import com.nh.travelmeetingapi.entity.Question;
import com.nh.travelmeetingapi.model.QuestionAnswer.QuestionAnswerChangeRequest;
import com.nh.travelmeetingapi.model.QuestionAnswer.QuestionAnswerCreateRequest;
import com.nh.travelmeetingapi.model.QuestionAnswer.QuestionAnswerItem;
import com.nh.travelmeetingapi.model.QuestionAnswer.QuestionAnswerResponse;
import com.nh.travelmeetingapi.model.generic.CommonResult;
import com.nh.travelmeetingapi.model.generic.ListResult;
import com.nh.travelmeetingapi.model.generic.SingleResult;
import com.nh.travelmeetingapi.service.QuestionAnswerService;
import com.nh.travelmeetingapi.service.QuestionService;
import com.nh.travelmeetingapi.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/question-answer")
public class QuestionAnswerController {
    private final QuestionService questionService;
    private final QuestionAnswerService questionAnswerService;

    @PostMapping("/new/{questionId}")
    public CommonResult setQuestionAnswer(@PathVariable long questionId, @RequestBody QuestionAnswerCreateRequest request){
        Question question = questionService.getData(questionId);
        questionAnswerService.setQuestionAnswer(question, request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    public ListResult<QuestionAnswerItem> getQuestionAnswers(){
        return ResponseService.getListResult(questionAnswerService.getQuestionAnswers());
    }

    @GetMapping("/detail/{questionAnswerId}")
    public SingleResult<QuestionAnswerResponse> getQuestionAnswer(@PathVariable long questionAnswerId){
        return ResponseService.getSingleResult(questionAnswerService.getQuestionAnswer(questionAnswerId));
    }

    @PutMapping("/change/{questionAnswerId}")
    public CommonResult putQuestionAnswer(@PathVariable long questionAnswerId, @RequestBody QuestionAnswerChangeRequest request){
        questionAnswerService.putQuestionAnswer(questionAnswerId, request);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/delete/{questionAnswerId}")
    public CommonResult delQuestionAnswer(@PathVariable long questionAnswerId){
        questionAnswerService.delQuestionAnswer(questionAnswerId);
        return ResponseService.getSuccessResult();
    }
}
