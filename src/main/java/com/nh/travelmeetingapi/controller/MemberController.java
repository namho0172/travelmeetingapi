package com.nh.travelmeetingapi.controller;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.model.generic.CommonResult;
import com.nh.travelmeetingapi.model.generic.ListResult;
import com.nh.travelmeetingapi.model.generic.SingleResult;
import com.nh.travelmeetingapi.model.member.*;
import com.nh.travelmeetingapi.model.member.userChange.*;
import com.nh.travelmeetingapi.repository.MemberRepository;
import com.nh.travelmeetingapi.service.MemberService;
import com.nh.travelmeetingapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/join") // 맴버 등록
    @Operation(summary = "회원 등록" )
    public CommonResult setMember(@RequestBody MemberJoinRequest request) throws Exception {
        memberService.setMember(request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/id/duplicate/check") // 아이디 중복 확인.
    @Operation(summary = "아이디 중복 확인" )
    public CommonResult getMemberIdDupCheck(@RequestParam(name = "account") String account) {
        memberService.getMemberIdDupCheck(account);
        return ResponseService.getSingleResult(account);
    }


    @GetMapping("/all") // 회원정보 모두 불러오기
    @Operation(summary = "회원 리스트 가져오기" )
    public ListResult<MemberItem> getMembers() {
        return ResponseService.getListResult(memberService.getMembers());
    }

    @GetMapping("/detail/{memberId}") // n번 회원 정보 불러오기
    @Operation(summary = "특정 회원 가져오기" )
    public SingleResult<MemberResponse> getMember(@PathVariable long memberId) {

        return ResponseService.getSingleResult(memberService.getMember(memberId));
    }

    @PutMapping("/memberId/{id}") // 회원 정보 수정 사실상 del를 가장한 put임
    @Operation(summary = "회원 정보 수정 del기능" )
    public CommonResult putMembers(@PathVariable long id, @RequestBody MemberChangeRequest request) {
        memberService.putMembers(id, request);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/member-name/{id}") // 회원 이름 수정
    @Operation(summary = "회원 이름 수정" )
    public CommonResult putMemberName(@PathVariable long id, @RequestBody MemberNameChangeRequest request) {
        memberService.putMemberName(id, request);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/member-pw/{id}") // 회원 비밀번호 수정
    @Operation(summary = "회원 비밀번호 수정" )
    public CommonResult putMemberPw(@PathVariable long id, @RequestBody MemberPasswordChangeRequest request) {
        memberService.putMemberPw(id, request);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/member-phone-number/{id}") // 회원 전화 번호 수정
    @Operation(summary = "회원 전화 번호 수정" )
    public CommonResult putMember(@PathVariable long id, @RequestBody MemberPhoneNumberChangeRequest request) {
        memberService.putMemberPn(id, request);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/member-email/{id}") //회원 이메일 수정
    @Operation(summary = "회원 이메일 수정" )
    public CommonResult putMember(@PathVariable long id, @RequestBody MemberEmailChangeRequest request) {
        memberService.putMemberEmail(id, request);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/member-address/{id}") // 아이디변경
    @Operation(summary = "아이디변경" )
    public CommonResult putMember(@PathVariable long id, @RequestBody MemberAddressChangeRequest request) {
        memberService.putMemberAd(id, request);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/delMember/{memberId}") // 회원 삭제
    @Operation(summary = "회원 삭제" )
    public CommonResult delMember(@PathVariable long memberId){
        memberService.delMember(memberId);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "페이지 불러오기" )
    public ListResult<Member> getPages(@PathVariable int pageNum){
        return memberService.getPages(pageNum);
    }
}
