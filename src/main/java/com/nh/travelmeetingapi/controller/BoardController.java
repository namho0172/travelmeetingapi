package com.nh.travelmeetingapi.controller;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.model.board.BoardCreateRequest;
import com.nh.travelmeetingapi.model.board.BoardItem;
import com.nh.travelmeetingapi.model.board.BoardResponse;
import com.nh.travelmeetingapi.model.generic.CommonResult;
import com.nh.travelmeetingapi.model.generic.ListResult;
import com.nh.travelmeetingapi.model.generic.SingleResult;
import com.nh.travelmeetingapi.service.BoardService;
import com.nh.travelmeetingapi.service.MemberService;
import com.nh.travelmeetingapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board")
public class BoardController {
    private final BoardService boardService;
    private final MemberService memberService;

    @PostMapping("/post/{memberId}") // 게시판 글 쓰기
    @Operation(summary = "게시판 등록")
    public CommonResult setBoard(@PathVariable long memberId, @RequestBody BoardCreateRequest request){
        Member member = memberService.getData(memberId);
        boardService.setBoard(member, request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    @Operation(summary = "게시판 리스트 가져오기")
    public ListResult<BoardItem> getBoards(){
        return ResponseService.getListResult(boardService.getBoards());
    }

    @GetMapping("/detail/{boardId}")
    @Operation(summary = "특정 게시판 가져오기")
    public SingleResult<BoardResponse> getBoard(@PathVariable long boardId){
        return ResponseService.getSingleResult(boardService.getBoard(boardId));
    }

    @DeleteMapping("/del/{boardId}")
    @Operation(summary = "게시판 삭제")
    public CommonResult delBoard(@PathVariable long boardId) {
        boardService.delBoard(boardId);
        return ResponseService.getSuccessResult();
    }
}
