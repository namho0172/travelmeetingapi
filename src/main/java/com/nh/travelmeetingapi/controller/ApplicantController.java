package com.nh.travelmeetingapi.controller;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.entity.PackageId;
import com.nh.travelmeetingapi.model.applicant.ApplicantItem;
import com.nh.travelmeetingapi.model.applicant.ApplicantRequest;
import com.nh.travelmeetingapi.model.applicant.ApplicantResponse;
import com.nh.travelmeetingapi.model.generic.CommonResult;
import com.nh.travelmeetingapi.model.generic.ListResult;
import com.nh.travelmeetingapi.model.generic.SingleResult;
import com.nh.travelmeetingapi.service.ApplicantService;
import com.nh.travelmeetingapi.service.MemberService;
import com.nh.travelmeetingapi.service.PackageIdService;
import com.nh.travelmeetingapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/applicant")
public class ApplicantController {
    private final ApplicantService applicantService;
    private final MemberService memberService;
   private final PackageIdService packageIdService;

    @PostMapping("/new/{memberId},{ppackageId}") // 신청자 등록
    @Operation(summary = "신청자 등록")
    public CommonResult setApplicant(@PathVariable long memberId,@PathVariable long ppackageId, @RequestBody ApplicantRequest request) throws Exception {
        Member member = memberService.getData(memberId);
        PackageId packageId = packageIdService.getData(ppackageId);
        applicantService.setApplicant(member,packageId,request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    @Operation(summary = "신청자 목록 가져오기")
    public ListResult<ApplicantItem> getApps(){
        return ResponseService.getListResult(applicantService.getApps());
    }

    @GetMapping("/detail/{appId}")
    @Operation(summary = "특정 신청자 가져오기")
    public SingleResult<ApplicantResponse> getApp(@PathVariable long appId){
        return ResponseService.getSingleResult(applicantService.getApp(appId));
    }

    @DeleteMapping("/delApp/{delAppId}")
    @Operation(summary = "신청자 지우기")
    public CommonResult delApp(@PathVariable long delAppId){
        applicantService.delApp(delAppId);
        return ResponseService.getSuccessResult();
    }
}
