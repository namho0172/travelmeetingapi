package com.nh.travelmeetingapi.controller;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.entity.matching.MatchingConfirmationConsent;
import com.nh.travelmeetingapi.model.generic.CommonResult;
import com.nh.travelmeetingapi.model.generic.ListResult;
import com.nh.travelmeetingapi.model.generic.SingleResult;
import com.nh.travelmeetingapi.model.pay.PayChangRequest;
import com.nh.travelmeetingapi.model.pay.PayItem;
import com.nh.travelmeetingapi.model.pay.PayRequest;
import com.nh.travelmeetingapi.model.pay.PayResponse;
import com.nh.travelmeetingapi.service.matching.MatchingConfirmationConsentService;
import com.nh.travelmeetingapi.service.MemberService;
import com.nh.travelmeetingapi.service.PayService;
import com.nh.travelmeetingapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pay")
public class PayController {
    private final MemberService memberService;
    private final PayService payService;
    private final MatchingConfirmationConsentService matchingConfirmationConsentService;

    @PostMapping("/new/{memberId}/{matchingId}")// 누가 어떤 패키지를 결제했는지 알 수 있게 맴버 와 매칭을 조인했음.
    @Operation(summary = "결제 등록" )
    public CommonResult setPay(@PathVariable long memberId,@PathVariable long matchingId,@RequestBody PayRequest request){
        Member member = memberService.getData(memberId);
        MatchingConfirmationConsent matchingConfirmationConsent = matchingConfirmationConsentService.getData(matchingId);
        payService.setPay(matchingConfirmationConsent,member,request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    @Operation(summary = "결제 리스트 가져오기" )
    public ListResult<PayItem> getPays(){
        return ResponseService.getListResult(payService.getPays());
    }
    @GetMapping("/detail/{payId}")
    @Operation(summary = "특정 결제 리스트 가져오기" )
    public SingleResult<PayResponse> getPay(@PathVariable long payId){
        return ResponseService.getSingleResult(payService.getPay(payId));

    }
    @PutMapping("/cancelId/{payId}")
    @Operation(summary = "결제 수정" )
    public CommonResult putPayCancel(@PathVariable long payId, @RequestBody PayChangRequest request){
        payService.putPayCancel(payId, request);

        return ResponseService.getSuccessResult();
    }

}
