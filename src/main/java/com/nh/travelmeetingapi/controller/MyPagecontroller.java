package com.nh.travelmeetingapi.controller;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.entity.pay.Pay;
import com.nh.travelmeetingapi.model.generic.CommonResult;
import com.nh.travelmeetingapi.model.myPage.MyPageRequest;
import com.nh.travelmeetingapi.service.MemberService;
import com.nh.travelmeetingapi.service.MyPageService;
import com.nh.travelmeetingapi.service.PayService;
import com.nh.travelmeetingapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/mypage")
public class MyPagecontroller {
    private final MyPageService myPageService;
    private final PayService payService;
    private final MemberService memberService;

    @PostMapping("/new/{memberId}/{payId}")
    @Operation(summary = "마이페이지 입력" )
    public CommonResult setMyPage(@PathVariable long memberId, @PathVariable long payId, @RequestBody MyPageRequest request){
        Member member = memberService.getData(memberId);
        Pay pay = payService.getData(payId);
        myPageService.setMyPage(member,pay,request);

        return ResponseService.getSuccessResult();
    }
}
