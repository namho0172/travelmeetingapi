package com.nh.travelmeetingapi.controller;

import com.nh.travelmeetingapi.entity.Member;
import com.nh.travelmeetingapi.model.Question.QuestionChangeRequest;
import com.nh.travelmeetingapi.model.Question.QuestionItem;
import com.nh.travelmeetingapi.model.Question.QuestionRequest;
import com.nh.travelmeetingapi.model.Question.QuestionResponse;
import com.nh.travelmeetingapi.model.generic.CommonResult;
import com.nh.travelmeetingapi.model.generic.ListResult;
import com.nh.travelmeetingapi.model.generic.SingleResult;
import com.nh.travelmeetingapi.service.MemberService;
import com.nh.travelmeetingapi.service.QuestionService;
import com.nh.travelmeetingapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/question")
public class QuestionController {
    private final QuestionService questionService;
    private final MemberService memberService;

    @PostMapping("/new/{memberId}")
    @Operation(summary = "1대1 문의" )
    public CommonResult setQuestion(@PathVariable long memberId, QuestionRequest request){
        Member member = memberService.getData(memberId);
        questionService.setQuestion(member, request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    @Operation(summary = "1대1 문의 리스트 가져오기" )
    public ListResult<QuestionItem> getQuestions(){
        return ResponseService.getListResult(questionService.getQuestions());
    }
    @GetMapping("/detail/{questionId}")
    @Operation(summary = "특정 1대1 문의 리스트 가져오기" )
    public SingleResult<QuestionResponse> getQuestion(@PathVariable long questionId){
        return ResponseService.getSingleResult(questionService.getQuestion(questionId));
    }

    @PutMapping("/change/{questionId}")
    @Operation(summary = "1대1 문의 삭제" )
    public CommonResult putQuestion(@PathVariable Long questionId, @RequestBody QuestionChangeRequest request){
        questionService.putQuestion(questionId, request);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/delQuestion/{deleteId}")
    public CommonResult delQuestion(@PathVariable long deleteId){
        questionService.delQuestion(deleteId);
        return ResponseService.getSuccessResult();
    }
}
