package com.nh.travelmeetingapi.controller;

import com.nh.travelmeetingapi.entity.Applicant;
import com.nh.travelmeetingapi.entity.PackageId;
import com.nh.travelmeetingapi.model.MatchingConfirmationConsent.MatchingCreateRequest;
import com.nh.travelmeetingapi.model.MatchingConfirmationConsent.MatchingItem;
import com.nh.travelmeetingapi.model.MatchingConfirmationConsent.MatchingResponse;
import com.nh.travelmeetingapi.model.generic.CommonResult;
import com.nh.travelmeetingapi.model.generic.ListResult;
import com.nh.travelmeetingapi.model.generic.SingleResult;
import com.nh.travelmeetingapi.service.ApplicantService;
import com.nh.travelmeetingapi.service.PackageIdService;
import com.nh.travelmeetingapi.service.matching.MatchingConfirmationConsentService;
import com.nh.travelmeetingapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/matching")
public class MatchingConfirmationConsentController {
    private final MatchingConfirmationConsentService matchingConfirmationConsentService;
    public final ApplicantService applicantService;
    public final PackageIdService packageIdService;

    @PostMapping("/new/{appId},{paId}") //  매칭 등록
    @Operation(summary = "확정자 등록")
    public CommonResult setMatching(@PathVariable long appId,@PathVariable long paId, @RequestBody MatchingCreateRequest request) {
        Applicant applicant = applicantService.getData(appId);
        PackageId packageId = packageIdService.getData(paId);
        matchingConfirmationConsentService.setMatching(applicant,packageId,request);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all") // 매칭 현황 확인
    @Operation(summary = "확정자 리스트 가져오기")
    public ListResult<MatchingItem> getMatchings(){
        return ResponseService.getListResult(matchingConfirmationConsentService.getMatchings());
    }

    @GetMapping("/detail/{matchingId}") // 특정 맴버의 매칭 현황 확인
    @Operation(summary = "특정 확정자 가져오기")
    public SingleResult<MatchingResponse> getMatching(@PathVariable long matchingId){//matchingId 주소Id와 같은 이름이여야 찾을 수 있다.

        return ResponseService.getSingleResult(matchingConfirmationConsentService.getMatching(matchingId));
    }


    @DeleteMapping("/delMatching/{matchingId}") // 매칭 신청 취소
    @Operation(summary = "확정자 삭제" )
    public CommonResult delMatching(@PathVariable long matchingId){
        matchingConfirmationConsentService.delMatching(matchingId);

        return ResponseService.getSuccessResult();
    }
}
