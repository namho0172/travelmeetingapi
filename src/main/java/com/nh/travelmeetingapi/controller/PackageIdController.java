package com.nh.travelmeetingapi.controller;

import com.nh.travelmeetingapi.model.Package.PackageItem;
import com.nh.travelmeetingapi.model.Package.PackageRequest;
import com.nh.travelmeetingapi.model.Package.PackageResponse;
import com.nh.travelmeetingapi.model.generic.CommonResult;
import com.nh.travelmeetingapi.model.generic.ListResult;
import com.nh.travelmeetingapi.model.generic.SingleResult;
import com.nh.travelmeetingapi.service.PackageIdService;
import com.nh.travelmeetingapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/packageId")
public class PackageIdController {
    private final PackageIdService packageIdService;

    @PostMapping("/new")
    @Operation(summary = "패키지 등록" )
    public CommonResult setPackage(@RequestBody PackageRequest request){
        packageIdService.setPackage(request);
        return ResponseService.getSuccessResult();
    }
    @GetMapping("/all/{packagesId}")
    @Operation(summary = "패키지 리스트 불러오기" )
    public ListResult<PackageItem> getPackages(){
        return ResponseService.getListResult(packageIdService.getPackages());
    }
    @GetMapping("/detail/{packageId}")
    @Operation(summary = "특정 패키지 불러오기" )
    public SingleResult<PackageResponse> getPackageOne(@PathVariable long packageId){
        return ResponseService.getSingleResult(packageIdService.getPackageOne(packageId));
    }
    @DeleteMapping("/del/{delPackageId}")
    @Operation(summary = "패키지 삭제" )
    public CommonResult delPackage(@PathVariable long delPackageId){
        packageIdService.delPackage(delPackageId);
        return ResponseService.getSuccessResult();
    }
}
