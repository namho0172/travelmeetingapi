package com.nh.travelmeetingapi.ilb;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public class CommonFile {// 패킷으로 쪼개져 받은 상태로 받는 multiPartFile 를 file로 조립하는 함수
    public static File mltipartToFile(MultipartFile multipartFile) throws IOException {
        File convFile = new File(System.getProperty("java.ip.tmpdir") + "/" + multipartFile.getOriginalFilename());
        //멀티파티 파일을 변환해서 저기에 넣으세요. convFile에 넣으세요.
        multipartFile.transferTo(convFile);//오버로딩 기능과 같다. 덮어쓴다.

        return convFile;
    }
}
